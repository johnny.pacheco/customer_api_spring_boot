package com.johnny.customers.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="customers")
@NamedQueries({
	@NamedQuery(name = "Customer.findAllsUpAge", query = "SELECT c FROM Customer c WHERE c.age >= :age")
})
public class Customer {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name="name", nullable=false, length=45)
	private String name;
	
	@Column(name="surname", nullable=false, length=45)
	private String surname;
	
	@Column(name="dni", nullable=false, length=15)
	private String dni;
	
	@Column(name="city", nullable=true, length=45)
	private String city;
	
	@Column(name="age", nullable=false)
	private Integer age;
	
	@Column(name="type_doc", nullable=false)
	private String typeDoc;
	
	@Column(name="img", nullable=false)
	@Lob
	private String img;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public String getTypeDoc() {
		return typeDoc;
	}
	public void setTypeDoc(String typeDoc) {
		this.typeDoc = typeDoc;
	}
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}

}
