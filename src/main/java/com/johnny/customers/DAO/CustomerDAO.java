package com.johnny.customers.DAO;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.johnny.customers.entity.Customer;

public interface CustomerDAO extends JpaRepository<Customer, Long> {
	
	List<Customer> findByTypeDoc(String typeDoc);
	
	List<Customer> findAllsUpAge(@Param("age") int age);

	
}
