package com.johnny.customers.repositories;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.johnny.customers.DAO.CustomerDAO;
import com.johnny.customers.entity.Customer;

@Repository
public class CustomerRepository {

	@Autowired
	private CustomerDAO customerDao;

	public List<Customer> getCustomers() {
		return parseCustomers(customerDao.findAll());
	}

	public List<Customer> getByTypeDoc(String typeDoc){
		return parseCustomers(customerDao.findByTypeDoc(typeDoc));
	}
	
	public Customer getById(Long id) {
		Optional<Customer> customer = customerDao.findById(id);
		
		if (customer.isPresent()) {
			return parserCustomer(customer.get());
		} else {
			return null;
		}
	}

	public Customer createCustomer(Customer client) {
		Customer newCust = customerDao.save(client);
		return parserCustomer(newCust);		
	}

	public void deleteCustomer(Long id) {
		customerDao.deleteById(id);
	}

	public Customer updateCustomer(Customer client) {
		Optional<Customer> customer = customerDao.findById(client.getId());

		if (customer.isPresent()) {
			Customer cust = customer.get();

			cust.setName(client.getName());
			cust.setAge(client.getAge());
			cust.setSurname(client.getSurname());
			cust.setDni(client.getDni());
			cust.setCity(client.getCity());
			cust.setTypeDoc(client.getTypeDoc());
			cust.setImg(client.getImg());
			
			customerDao.save(cust);

			return parserCustomer(cust);

		} else {
			return null;
		}
	}
	
	public List<Customer> getAllsUpAge(Integer age){
		return parseCustomers(customerDao.findAllsUpAge(age));
	}
	
	public String getBase64(Long id) {
		Optional<Customer> cust = customerDao.findById(id);
		
		if(cust.isPresent() && !cust.get().getImg().isEmpty()) {
			
			return cust.get().getImg();
		}else {
			return null;
		}
	}
	
	private Customer parserCustomer(Customer c) {
		c.setImg("/customer/image?id="+c.getId());
		return c;
	}
	
	private List<Customer> parseCustomers(List<Customer> clients ){
		List<Customer> customers = new ArrayList<Customer>();
		for(Customer cust : clients) {
			customers.add(parserCustomer(cust));
		}
		return customers;
	}
}
